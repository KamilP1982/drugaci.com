from django.conf.urls import url
from . import views

urlpatterns = [
             url(r'^$', views.index, name='index'),
             url(r'^index$', views.index, name='index'),
             url(r'^releases$', views.releases, name='releases'),
			 url(r'^release/(?P<release_name>[\w|\W]+)/$', views.release, name='release'),
		     url(r'^artists$', views.artists, name='artists'),
             ]
