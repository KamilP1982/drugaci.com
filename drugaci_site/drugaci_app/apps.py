from django.apps import AppConfig


class DrugaciAppConfig(AppConfig):
    name = 'drugaci_app'
