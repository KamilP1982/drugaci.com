from django.contrib import admin
from .models import Release, Track, Artist


admin.site.register(Release)
admin.site.register(Track)
admin.site.register(Artist)
