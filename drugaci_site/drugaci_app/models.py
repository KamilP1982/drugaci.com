#from django.contrib.auth.models import Permission, User
from django.db import models

class Artist(models.Model):
    artist_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    photo = models.FileField(default='')
    bio = models.TextField(blank=True, null=True)
    def __str__(self):
        return self.name


class Release(models.Model):
    catalog = models.CharField(max_length=20,default='DEX##2017')
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    release_title = models.CharField(max_length=500)
    release_logo = models.FileField(default='')
    is_new = models.BooleanField(default=False)

    def __str__(self):
        return self.release_title 


class Track(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    release_name = models.ForeignKey(Release, on_delete=models.CASCADE)
    track_title = models.CharField(max_length=250)
    audio_file = models.FileField(default='')
    release_waveform = models.FileField(default='')
    is_new = models.BooleanField(default=False)

    def __str__(self):
        return self.track_title

