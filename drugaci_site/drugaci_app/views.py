from django.shortcuts import render
from django.template import Template,Context
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.http import JsonResponse,HttpResponse
from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from django.forms.models import model_to_dict
import json,os
from .models import Release, Track, Artist
from django.contrib.staticfiles.storage import staticfiles_storage

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')

def index(request):
    releases = Release.objects.all()
    track_results = Track.objects.all()
    query = request.GET.get("q")
    page_title = "drugaci_app/main.html"

    return render(request, 'drugaci_app/index.html', {
                            'page_title': page_title,
                          })

#@render_to('rugaci_app/display_box.html')#annoying-decorator
def releases(request):
    #releases = model_to_dict(Release.objects.values())
    #releases = model_to_dict(Release.objects.filter(id=1).values())
    #release = { 'test' : "this is a test"}

    #query = request.GET.get("q")
    #release = release.filter(Q(release_title__icontains=query) | Q(artist__icontains=query)).distinct()
    p = staticfiles_storage.path('drugaci_app/artists.html')
    f = open(PROJECT_DIR + "/templates/drugaci_app/artists.html",'r');
    text = f.read()
    template = Template(text)
    context = Context({})
    html = template.render(context);
    #content = p.readlines()
    page_title = "drugaci_app/releases.html"
    data = {'dude': Release.objects.values()}
    #data['error_message'] = 'A user with this username already exists.'
    #serialized_q = json.dumps(data)
    #if query:
        #release = release.filter(Q(release_title__icontains=query) | Q(artist__icontains=query)).distinct()
  
        #return JsonResponse(json.dumps(release))
    #else:
    return HttpResponse(html) 

def artists(request):
    artists = Artist.objects.all()
    track_results = Track.objects.all()
    query = request.GET.get("q")
    page_title = "drugaci_app/artists.html"
    if query:
        release = release.filter(Q(release_title__icontains=query) | Q(artist__icontains=query)).distinct()
        track_results = track_results.filter(Q(track_title__icontains=query)).distinct()

        return render(request, 'drugaci_app/index.html', {
			       'artists': artists,
			       'page_title': page_title,
		  		})
    else:
        return render(request, 'drugaci_app/index.html', {
                               'artists': artists,
                               'page_title': page_title,
                               })

def release(request, release_name):
    release = Release.objects.get(release_title=release_name)
    tracks_results = Track.objects.filter(release_name_id=release)
    #track_results = Track.objects.all()
    query = request.GET.get("q")
    page_title = "drugaci_app/release.html"
    if query:
        release = release.filter(Q(release_title__icontains=query) | Q(artist__icontains=query)).distinct()
        track_results = track_results.filter(Q(track_title__icontains=query)).distinct()

        return render(request, 'drugaci_app/index.html', {
			       'release': release,
                   'tracks_results': tracks_results,
			       'page_title': page_title,
		  		})
    else:
        return render(request, 'drugaci_app/index.html', {
                               'release': release,
                               'tracks_results': tracks_results,
			                   'page_title': page_title,
                               })
