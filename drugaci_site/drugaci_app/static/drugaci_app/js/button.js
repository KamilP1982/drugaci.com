//This is a jQuery script for player
var audioElement = document.createElement('audio');
$(window).load(function(){
     $('#player_container').show();
});

$(document).ready(function() {
	//show_player();
    $('[id=release]').click(show_player);
    var left = $("#bottom").offset().left;
    var top = $("#bottom").offset().top;
    $("#progress_slider").offset({ top: top, left: left});
    $("#progress_slider").draggable({axis: "x" },
                		    {containment: "#bottom" },
			            { drag: function( event, ui ) {
                                      startPos = $("#bottom").offset().left;
				      
                                      var posX = $(this).offset().left;
                                      console.log(posX);
                                      position = posX-startPos;
				      $("#top").css('clip', 'rect(0px, '+position+'px, 73px, 0px)');
                                     }}
);
});

function show_player()
{
    $('#player_container').slideDown(100);
	var audio_url = $('#filename').text();
	audioElement.setAttribute('src', audio_url);
	console.log(audio_url);
	play();	
}


