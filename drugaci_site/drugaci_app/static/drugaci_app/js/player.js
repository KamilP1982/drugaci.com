//This is a jQuery script for player

$(document).ready(function() {
    $('#pause').hide();
    $('#pause').click(pause);
    $('#play').click(play);	
});

function play()
{
    $('#pause').show();
    $('#play').hide();
	audioElement.play();
}

function pause()
{
    $('#pause').hide();
    $('#play').show();
	audioElement.pause();
}

